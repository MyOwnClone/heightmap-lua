require "heightmap"

if arg and arg[#arg] == "-debug" then require("mobdebug").start() end
random=math.random

width = 500
height= 500

function randomizedFunc(map, x, y, d, h)
    return h + (random()-0.5)*d
end

function determFunct(map, x, y, d, h)
    return h + d
end

function transformationFunc(map, x, y, d, h)
    return determFunct(map, x, y, d, h)
end

function generate(width, height, funcRef)
  map = heightmap.create(width, height, funcRef)
  
  data = love.image.newImageData(width, height)
  for i=0, (width-1) do   
    for j=0,(height-1) do
      value = map[i][j]  
      data:setPixel(i, j, value, value, value, 255)
    end
  end
  
  img = love.graphics.newImage(data)  
end

function love.load()  
  love.window.setMode( 1280, 800 )
  love.window.setTitle( "diamond square algorithm" )
  generate(width, height,transformationFunc)
end

function love.keypressed(key)
  if key == "escape" then
    love.event.quit()
  elseif key=="return" then
    generate(width, height,transformationFunc)
  end
end

function love.draw()  
  love.graphics.draw(img, love.graphics.newQuad( 0, 0, width, height, width, height ))
end
